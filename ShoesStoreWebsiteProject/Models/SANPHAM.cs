﻿namespace ShoesStoreWebsiteProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SANPHAM")]
    public partial class SANPHAM
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SANPHAM()
        {
            CHITIETGIAYs = new HashSet<CHITIETGIAY>();
        }

        [Key]
        public int Magiay { get; set; }

        [Required]
        [StringLength(100)]
        public string Tengiay { get; set; }

        public decimal? Giaban { get; set; }

        [StringLength(50)]
        public string Anhbia { get; set; }

        public DateTime? Ngaycapnhat { get; set; }

        public int? MaNCC { get; set; }

        public int? Maloai { get; set; }

        public bool? Trangthai { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CHITIETGIAY> CHITIETGIAYs { get; set; }

        public virtual LOAIGIAY LOAIGIAY { get; set; }

        public virtual NHACUNGCAP NHACUNGCAP { get; set; }

        //=======================giá trị ngoài===============================

        public string tenNhaCungCap;
        public string loaiGiay;
        public int maNhaCungCap;
        public List<int?> sizeGiay;
        public List<int> maSize;
        public List<int?> soluongton;
        public bool tinhTrangSanPham; // hết hàng hoặc còn hàng => số lượng tồn = 0
        public List<NHACUNGCAP> listnhaCungCap;
        public List<LOAIGIAY> listloaiGiay;
        public string urlBack; // khi nhấn vào button trở lại ở productDetail thì sẽ trả lại link dựa vào biến urlBack
        public string searchingValue; // lưu giá trị mà người dùng nhập vào ô tìm kiếm sản phẩm (action: SearchProduct)
    }
}
