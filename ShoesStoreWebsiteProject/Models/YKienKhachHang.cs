﻿namespace ShoesStoreWebsiteProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("YKienKhachHang")]
    public partial class YKienKhachHang
    {
        [Key]
        public int MaYKien { get; set; }

        [StringLength(255)]
        public string HoTen { get; set; }

        [StringLength(255)]
        public string Email { get; set; }

        public DateTime? NgayGui { get; set; }

        public string NoiDung { get; set; }

        public bool? TrangThai { get; set; }
    }

    public partial class YKienKhachHang
    {
        public string urlBack; // trở lại trang trước khi nhấn vào button xem chi tiết (lưu số trang lúc nhấn vào)
    }
}
