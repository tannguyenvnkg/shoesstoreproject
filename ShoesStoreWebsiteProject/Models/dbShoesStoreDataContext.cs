using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace ShoesStoreWebsiteProject.Models
{
    public partial class dbShoesStoreDataContext : DbContext
    {
        public dbShoesStoreDataContext()
            : base("name=dbShoesStoreDataContext")
        {
        }

        public virtual DbSet<CHITIETDONHANG> CHITIETDONHANGs { get; set; }
        public virtual DbSet<CHITIETGIAY> CHITIETGIAYs { get; set; }
        public virtual DbSet<DONDATHANG> DONDATHANGs { get; set; }
        public virtual DbSet<KHACHHANG> KHACHHANGs { get; set; }
        public virtual DbSet<LOAIGIAY> LOAIGIAYs { get; set; }
        public virtual DbSet<NHACUNGCAP> NHACUNGCAPs { get; set; }
        public virtual DbSet<SANPHAM> SANPHAMs { get; set; }
        public virtual DbSet<SIZEGIAY> SIZEGIAYs { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<QUANLY> QUANLies { get; set; }
        public virtual DbSet<YKienKhachHang> YKienKhachHangs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CHITIETDONHANG>()
                .Property(e => e.Dongia)
                .HasPrecision(18, 0);

            modelBuilder.Entity<CHITIETGIAY>()
                .HasMany(e => e.CHITIETDONHANGs)
                .WithRequired(e => e.CHITIETGIAY)
                .HasForeignKey(e => new { e.Magiay, e.Masize })
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DONDATHANG>()
                .Property(e => e.Tongtien)
                .HasPrecision(18, 0);

            modelBuilder.Entity<DONDATHANG>()
                .HasMany(e => e.CHITIETDONHANGs)
                .WithRequired(e => e.DONDATHANG)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<KHACHHANG>()
                .Property(e => e.Taikhoan)
                .IsUnicode(false);

            modelBuilder.Entity<KHACHHANG>()
                .Property(e => e.Matkhau)
                .IsUnicode(false);

            modelBuilder.Entity<KHACHHANG>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<KHACHHANG>()
                .Property(e => e.DienthoaiKH)
                .IsUnicode(false);

            modelBuilder.Entity<NHACUNGCAP>()
                .Property(e => e.DienThoai)
                .IsUnicode(false);

            modelBuilder.Entity<SANPHAM>()
                .Property(e => e.Giaban)
                .HasPrecision(18, 0);

            modelBuilder.Entity<SANPHAM>()
                .Property(e => e.Anhbia)
                .IsUnicode(false);

            modelBuilder.Entity<SANPHAM>()
                .HasMany(e => e.CHITIETGIAYs)
                .WithRequired(e => e.SANPHAM)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SIZEGIAY>()
                .HasMany(e => e.CHITIETGIAYs)
                .WithRequired(e => e.SIZEGIAY)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<QUANLY>()
                .Property(e => e.Taikhoan)
                .IsUnicode(false);

            modelBuilder.Entity<QUANLY>()
                .Property(e => e.Matkhau)
                .IsUnicode(false);

            modelBuilder.Entity<YKienKhachHang>()
                .Property(e => e.Email)
                .IsUnicode(false);
        }
    }
}
