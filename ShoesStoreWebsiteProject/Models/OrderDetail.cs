﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShoesStoreWebsiteProject.Models
{
    public class OrderDetail
    {
        public int? iShoesID { get; set; }
        //public int? iSizeID { get; set; }
        public string sShoesName { get; set; }
        public int? iShoesSize { get; set; }
        public string sImageCover { get; set; }
        public Decimal dCost { get; set; } 
        public Double dTotalMoney
        {
            get { return (double)(iAmount * dCost); }
        }
        //===============giá trị phải tự gán====================================
        public decimal? dTotalbill; 
        public int iAmount { get; set; } // số lượng

        public OrderDetail(int? shoesID, int? sizeID,int? orderID)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            iShoesID = shoesID;
            //iSizeID = sizeID;
            iOrderID = orderID;
            SANPHAM product = db.SANPHAMs.Single(n => n.Magiay == shoesID);
            sShoesName = product.Tengiay;
            sImageCover = product.Anhbia;
            if(iOrderID == null) dCost = (decimal)product.Giaban; // gán giá bán bên table sản phẩm
            else
            {
                var cost = db.CHITIETDONHANGs.Single(p => p.Magiay == iShoesID && p.Masize == sizeID && p.MaDonHang == iOrderID); // gán giá bán bên chi tiết đơn hàng => khi thay đổi giá sản phẩm thì vẫn lưu giá cũ của đơn hàng cũ
                dCost = (decimal)cost.Dongia;
            }

            var shoesSize = db.SIZEGIAYs.FirstOrDefault(p => p.Masize == sizeID);
            iShoesSize = shoesSize.Sosize;

            var order = db.DONDATHANGs.SingleOrDefault(p => p.MaDonHang == orderID);
            if (order != null) iUserID = order.MaKH;
        }

        //=================giá trị bên controller admin===============
        public int? iOrderID { get; set; }
        public int? iUserID { get; set; } // action orderdetail
        public string urlBackToListOrder { get; set; } // nhấn vào chi tiết 1 đơn hàng thì giá trị này sẽ gán vào button trở lại (phần danh sách đơn hàng admin) 
    }
}