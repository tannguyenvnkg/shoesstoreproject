namespace ShoesStoreWebsiteProject.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("QUANLY")]
    public partial class QUANLY
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(255)]
        public string Taikhoan { get; set; }

        //[Key]
        [Column(Order = 1)]
        [StringLength(255)]
        public string Matkhau { get; set; }

        [StringLength(255)]
        public string Hoten { get; set; }

        public bool? Chusohuu { get; set; }

        //[Key]
        [Column(Order = 2)]
        public bool Trangthai { get; set; }
    }
}
