﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ShoesStoreWebsiteProject.Models;
namespace ShoesStoreWebsiteProject.Models
{
    public class Cart
    {
        public int? iShoesID { get; set; }
        public int? iSizeID { get; set; }
        public string sShoesName { get; set; }
        public string sImageCover { get; set; }
        public int iAmount { get; set; }
        public Double dCost { get; set; }
        public Double dTotalMoney
        {
            get { return iAmount * dCost; }
        }
        public int? iShoesSize { get; set; }
        public int? imaxAmount { get; set; } // sồ lượng tồn

        public Cart(int? shoesID, int? sizeID)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            iShoesID = shoesID;
            iSizeID = sizeID;
            SANPHAM product = db.SANPHAMs.Single(n => n.Magiay == iShoesID);
            sShoesName = product.Tengiay;
            sImageCover = product.Anhbia;
            dCost = double.Parse(product.Giaban.ToString());
            iAmount = 1;

            var shoesSize = db.SIZEGIAYs.FirstOrDefault(p => p.Masize == iSizeID);
            iShoesSize = shoesSize.Sosize;

            var maxAmount = db.CHITIETGIAYs.FirstOrDefault(p => p.Magiay == iShoesID && p.Masize == iSizeID);
            imaxAmount = maxAmount.Soluongton;
        }
        
    }
}