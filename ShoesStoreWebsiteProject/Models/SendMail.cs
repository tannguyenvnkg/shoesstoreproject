﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Mail;

namespace ShoesStoreWebsiteProject.Models
{   
    public class SendMail
    {
        private string tenKhachHang;
        private string emailKhachHang;
        private int maDonHang;
        private DateTime ngayGiaoHang;
        private decimal tongTien;

        public SendMail()
        {
            tenKhachHang = "";
            emailKhachHang = "";
            maDonHang = 0;
            ngayGiaoHang = DateTime.Now;
            tongTien = 0;
        }

        public SendMail(string tenKhachHang, string emailKhachHang, int maDonHang, DateTime ngayGiaoHang, decimal tongTien)
        {
            this.tenKhachHang = tenKhachHang;
            this.emailKhachHang = emailKhachHang;
            this.maDonHang = maDonHang;
            this.ngayGiaoHang = ngayGiaoHang;
            this.tongTien = tongTien;
        }

        public string TenKhachHang { get => tenKhachHang; set => tenKhachHang = value; }
        public string EmailKhachHang { get => emailKhachHang; set => emailKhachHang = value; }
        public int MaDonHang { get => maDonHang; set => maDonHang = value; }
        public DateTime NgayGiaoHang { get => ngayGiaoHang; set => ngayGiaoHang = value; }
        public decimal TongTien { get => tongTien; set => tongTien = value; }

        //hàm gửi mail nè 
        public void sendMail(string emailKhachHang, string tenKhachHang, string maDonHang, string ngayGiaoHang, decimal tongTien)
        {
            string email = "inazumaqiqi@gmail.com";

            var senderEmail = new MailAddress(email, "Teacup Shoes Store (tin nhắn tự động)");
            var receiverEmail = new MailAddress(emailKhachHang, tenKhachHang);
            var password = "fxjfpulvdnahfmpj";
            var sub = "Cảm ơn bạn đã đặt hàng Teacup Shoes Store";
            var body = "Cảm ơn " +tenKhachHang+ " đã đặt hàng \n" + "Mã đơn hàng của bạn là: " + maDonHang + "\nNgày giao hàng: " + ngayGiaoHang + "\nTổng tiền của quý khách hàng: " + String.Format("{0:0,0}", tongTien) + " VND";
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(senderEmail.Address, password)
            };
            using (var mess = new MailMessage(senderEmail, receiverEmail)
            {
                Subject = sub,
                Body = body
            })
            {
                smtp.Send(mess);
            }
        }
    }
}