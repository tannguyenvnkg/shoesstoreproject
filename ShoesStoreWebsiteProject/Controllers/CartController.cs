﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShoesStoreWebsiteProject.Models;

namespace WebBanGiay.Controllers
{
    public class CartController : Controller
    {
        // GET: Cart

        public List<Cart> GetCart() // tạo list giỏ hàng hoặc lấy giỏ hàng
        {
            List<Cart> listProductInCart = Session["Cart"] as List<Cart>;
            if (listProductInCart == null)
            {
                listProductInCart = new List<Cart>();
                Session["Cart"] = listProductInCart;
            }
            return listProductInCart;
        }
        private int TotalProduct() // lấy tổng số lượng trong giỏ hàng
        {
            int iAmount = 0;
            List<Cart> listProductInCart = Session["Cart"] as List<Cart>;
            if (listProductInCart != null)
            {
                iAmount = listProductInCart.Sum(n => n.iAmount);
            }
            return iAmount;
        }

        private double TotalMoney() // lấy tổng tiền của giỏ hàng
        {
            double iTotalMoney = 0;
            List<Cart> listProductInCart = Session["Cart"] as List<Cart>;
            if (listProductInCart != null)
            {
                iTotalMoney = listProductInCart.Sum(n => n.dTotalMoney);
            }
            return iTotalMoney;
        }

        private void updateAmount(CHITIETDONHANG ctdh) // cập nhật số lượng tồn của mỗi giày
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var shoes = db.CHITIETGIAYs.Single(p => p.Magiay == ctdh.Magiay && p.Masize == ctdh.Masize);
            shoes.Soluongton = shoes.Soluongton - ctdh.Soluong;
            db.SaveChanges();
        }

        //============================================           ACTION              ========================================================================
        [HttpPost]
        public ActionResult AddCart(int? iShoesID, string strURl ,FormCollection collection) // thêm sản phẩm vào giỏ hàng
        {
            int? sizeid = null;
            List<Cart> listProductInCart = GetCart();
            //int? sizeid = Int32.Parse(iSizeID);
            if (Request.Form["maSize"] == null)
            {
                ViewBag.ChooseSize = "Vui Lòng Chọn Size Giày";
                return Redirect(strURl);
            }
            sizeid = Int32.Parse(Request.Form["maSize"].ToString());
            Cart product = listProductInCart.Find(n => n.iShoesID == iShoesID && n.iSizeID == sizeid);
            if (product == null)
            {
                product = new Cart(iShoesID,sizeid);
                listProductInCart.Add(product);
                return Redirect(strURl);
            }
            else
            {
                product.iAmount++;
                return Redirect(strURl);
            }
        }

        public ActionResult Cart() // giỏ hàng
        {
            List<Cart> listProductInCart = GetCart();
            if (listProductInCart.Count == 0)
            {
                return RedirectToAction("ListProduct", "Product");
            }
            ViewBag.TotalProduct = TotalProduct();
            ViewBag.TotalMoney = TotalMoney();
            return View(listProductInCart);
        }

        public ActionResult _CartPartial() // partial giỏ hàng
        {
            ViewBag.TotalProduct = TotalProduct();
            ViewBag.TotalMoney = TotalMoney();
            return PartialView();
        }

        public ActionResult RemoveItemInCart(int iShoesID, int iSizeID) // xóa 1 món hàng ra khỏi giỏ hàng
        {
            List<Cart> listProductInCart = GetCart();
            Cart product = listProductInCart.SingleOrDefault(n => n.iShoesID == iShoesID && n.iSizeID == iSizeID);
            if (product != null)
            {
                listProductInCart.RemoveAll(n => n.iShoesID == iShoesID && n.iSizeID == iSizeID);
                return RedirectToAction("Cart");
            }
            if (listProductInCart.Count == 0)
            {
                return RedirectToAction("ListProduct", "Product");
            }
            return RedirectToAction("Cart");
        }

        public ActionResult UpdateItemInCart(int iShoesID, int iSizeID, FormCollection collection) // cập nhật lại số lượng của sản phẩm trong giỏ hàng
        {
            List<Cart> listProductInCart = GetCart();
            Cart product = listProductInCart.SingleOrDefault(n => n.iShoesID == iShoesID && n.iSizeID == iSizeID);
            if (product != null)
            {
                product.iAmount = int.Parse(collection["amount"].ToString());
            }
            return RedirectToAction("Cart");
        }

        public ActionResult Pay(string strURL) // thanh toán
        {
            if (Session["User"] == null || Session["User"].ToString() == "")
                return RedirectToAction("Login", "User", new { @strURL = strURL }); // truyền url để lưu trang web quay về sau khi login
            if (Session["Cart"] == null)
                return RedirectToAction("ListProduct", "Product");

            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            DONDATHANG ddh = new DONDATHANG();
            KHACHHANG kh = (KHACHHANG)Session["User"];
            List<Cart> gh = GetCart();
            ddh.MaKH = kh.MaKH;
            ddh.Ngaydat = DateTime.Now;
            ddh.Ngaygiao = DateTime.Now.AddDays(3);
            ddh.Tinhtranggiaohang = false;
            ddh.Dathanhtoan = false;
            ddh.Tongtien = (decimal?)TotalMoney();
            db.DONDATHANGs.Add(ddh);
            db.SaveChanges();

            foreach (var item in gh)
            {
                CHITIETDONHANG ctdh = new CHITIETDONHANG();
                ctdh.MaDonHang = ddh.MaDonHang;
                ctdh.Magiay = (int)item.iShoesID;
                ctdh.Masize = (int)item.iSizeID;
                ctdh.Soluong = item.iAmount;
                ctdh.Dongia = (decimal)item.dCost;
                updateAmount(ctdh); // cập nhật số lượng tồn
                db.CHITIETDONHANGs.Add(ctdh);
            }
            db.SaveChanges();

            SendMail sendmail = new SendMail();
            sendmail.sendMail(kh.Email, kh.HoTen, ddh.MaDonHang.ToString(), String.Format("{0:dd/MM/yyyy}", ddh.Ngaygiao),  ddh.Tongtien.Value);

            Session["Cart"] = null;
            return RedirectToAction("ConfirmOrder", "Cart");
        }

        public ActionResult RemoveCart()
        {
            List<Cart> listProductInCart = GetCart();
            listProductInCart.Clear();
            return RedirectToAction("ListProduct", "Product");
        }

        public ActionResult ConfirmOrder()
        {
            return View();
        }
    }
}