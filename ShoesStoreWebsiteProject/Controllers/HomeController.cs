﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShoesStoreWebsiteProject.Models;
namespace WebBanGiay.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Home()
        {
            return View();
        }
        public ActionResult AboutUs()
        {
            return View();
        }
        public ActionResult ContactUs()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SendComment(FormCollection collection)
        {
            var firstName = collection["firstName"].Trim();
            var lastName = collection["lastName"].Trim();
            var email = collection["email"].Trim();
            var comment = collection["comment"].Trim();

            if(String.IsNullOrEmpty(firstName) || String.IsNullOrEmpty(lastName) || String.IsNullOrEmpty(email) || String.IsNullOrEmpty(comment))
            {
                ViewBag.Error = "Vui lòng điền đủ thông tin";
                return View("ContactUs");
            }
            var name = firstName + " " + lastName; // nối tên
            // loại bỏ các ký tự trong tên
            char[] charsToTrim = { '*', '.' ,',' ,';', ':', '"', '+', '=', '-', '_', '/', '|', '?', '>', '<', '~', '`', '@', '#', '$', '%', '^', '&', '(', ')', '[', ']', '{', '}' };
            name = name.Trim(charsToTrim);

            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var guestComment = new YKienKhachHang();

            guestComment.HoTen = name;
            guestComment.Email = email;
            guestComment.NgayGui = DateTime.Now;
            guestComment.NoiDung = comment;
            guestComment.TrangThai = false; // trạng thái = false có nghĩa là quản trị viên chưa đọc

            db.YKienKhachHangs.Add(guestComment);
            db.SaveChanges();
            ViewBag.Success = "Gửi Thành Công";
            return View("ContactUs");
        }
    }
}