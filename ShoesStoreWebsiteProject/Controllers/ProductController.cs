﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShoesStoreWebsiteProject.Models;

using PagedList;
using PagedList.Mvc;
namespace WebBanGiay.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult ListProduct(int? page)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var listProduct = db.SANPHAMs.Where(p=>p.Trangthai == true).ToList();
            foreach (var item in listProduct)
            {
                var quantityInStock = item.CHITIETGIAYs.Where(p => p.Magiay == item.Magiay).Sum(p => p.Soluongton); // đếm số lượng tồn của từng sản phẩm
                // nếu còn hàng thì true hết hàng thì false
                if (quantityInStock == 0) item.tinhTrangSanPham = false;
                else item.tinhTrangSanPham = true; 
            }
            listProduct = listProduct.OrderByDescending(p => p.tinhTrangSanPham).ToList();

            //===================phân trang======================
            int pageSize = 8; // mỗi trang 8 sản phẩm
            int pageNum = (page ?? 1); // nếu page = null => pageNum = 1

            return View(listProduct.ToPagedList(pageNum,pageSize));
        }

        [HttpGet]
        public ActionResult ProductDetail(int? id, string url)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var product = db.SANPHAMs.FirstOrDefault(p => p.Magiay == id);

            var nhaCungCap = db.NHACUNGCAPs.FirstOrDefault(p => p.MaNCC == product.MaNCC);
            var maSize = db.CHITIETGIAYs.Where(p => p.Magiay == id).Select(p => p.Masize).ToList();
            var soLuongTon = db.CHITIETGIAYs.Where(p => p.Magiay == id).Select(p => p.Soluongton).ToList();
            var sizegiay = db.SIZEGIAYs.Select(p => p.Sosize).ToList();
            var demSanPham = soLuongTon.Sum(p => p.Value); // đếm list số lượng tồn của các size
            if (nhaCungCap != null)
            {
                product.maNhaCungCap = nhaCungCap.MaNCC;
                product.tenNhaCungCap = nhaCungCap.TenNCC;
                product.maSize = maSize;
                product.soluongton = soLuongTon;
                product.sizeGiay = sizegiay;
            }
            if (demSanPham == 0) product.tinhTrangSanPham = false;
            else product.tinhTrangSanPham = true;
            if (!String.IsNullOrEmpty(url)) product.urlBack = url;
            return View(product);
        }

        public ActionResult _ProductWithTrademark() // show navigation chọn sản phẩm theo thương hiệu (nhà cung cấp)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var product = db.NHACUNGCAPs.ToList();
            return PartialView(product);
        }
        public ActionResult _ProductWithShoesType() // show navigation chọn sản phẩm theo loại giày
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var product = db.LOAIGIAYs.ToList();
            return PartialView(product);
        }

        public ActionResult ProductWithTrademark(int? id,int? page) // show giày theo mã nhà cung cấp
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var listProduct = db.SANPHAMs.Where(p => p.MaNCC == id && p.Trangthai == true).ToList();

            foreach (var item in listProduct)
            {
                var quantityInStock = item.CHITIETGIAYs.Where(p => p.Magiay == item.Magiay).Sum(p => p.Soluongton); // đếm số lượng tồn của từng sản phẩm
                // nếu còn hàng thì true hết hàng thì false
                if (quantityInStock == 0) item.tinhTrangSanPham = false;
                else item.tinhTrangSanPham = true;
            }

            //===================phân trang======================
            int pageSize = 8; // mỗi trang 8 sản phẩm
            int pageNum = (page ?? 1); // nếu page = null => pageNum = 1
            return View(listProduct.ToPagedList(pageNum, pageSize));
        }
        public ActionResult ProductWithShoesType(int? id, int? page) // show giày theo mã loại
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var listProduct = db.SANPHAMs.Where(p => p.Maloai == id && p.Trangthai == true).ToList();

            foreach (var item in listProduct)
            {
                var quantityInStock = item.CHITIETGIAYs.Where(p => p.Magiay == item.Magiay).Sum(p => p.Soluongton); // đếm số lượng tồn của từng sản phẩm
                // nếu còn hàng thì true hết hàng thì false
                if (quantityInStock == 0) item.tinhTrangSanPham = false;
                else item.tinhTrangSanPham = true;
            }
            //===================phân trang======================
            int pageSize = 8; // mỗi trang 8 sản phẩm
            int pageNum = (page ?? 1); // nếu page = null => pageNum = 1

            //return View(listProduct);
            return View(listProduct.ToPagedList(pageNum, pageSize));
        }

        public ActionResult SearchProduct(int? page,string value,FormCollection f)
        {
            var search = f["search"];
            string searchingValue = "";
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            List<SANPHAM> listProduct = null;
            if (!String.IsNullOrEmpty(search))
            {
                listProduct = db.SANPHAMs.Where(p => p.Tengiay.Contains(search) && p.Trangthai == true).ToList();// nếu người dùng nhập vào ô tìm kiếm thì listProduct sẽ xuất giá trị theo biến search 
                searchingValue = search;
            }
            else
            {
                listProduct = db.SANPHAMs.Where(p => p.Tengiay.Contains(value) && p.Trangthai == true).ToList(); // giá trị sẽ chạy khi người dùng đã nhập vào ô tìm kiếm sản phẩm và click sang những trang tiếp theo
                searchingValue = value;
            }

            foreach (var item in listProduct)
            {
                var quantityInStock = item.CHITIETGIAYs.Where(p => p.Magiay == item.Magiay).Sum(p => p.Soluongton); // đếm số lượng tồn của từng sản phẩm
                // nếu còn hàng thì true hết hàng thì false
                if (quantityInStock == 0) item.tinhTrangSanPham = false;
                else item.tinhTrangSanPham = true;

                item.searchingValue = searchingValue; // lưu giá trị này để người dùng truy cập vào những trang tiếp theo sẽ hiện
            }

            listProduct = listProduct.OrderByDescending(p => p.tinhTrangSanPham).ToList();
            //===================phân trang======================
            int pageSize = 8; // mỗi trang 8 sản phẩm
            int pageNum = (page ?? 1); // nếu page = null => pageNum = 1

            return View(listProduct.ToPagedList(pageNum, pageSize));
        }
    }
}