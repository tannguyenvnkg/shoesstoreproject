﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShoesStoreWebsiteProject.Models;

namespace ShoesStoreWebsiteProject.Controllers
{
    public class OrderController : Controller
    {
        private static string urlBeforeClickingHistoryOrder; // lưu đường dẫn trước khi nhấn vào lịch sử mua hàng
        public ActionResult OrderHistory(string strURL)
        {
            if (strURL != null) urlBeforeClickingHistoryOrder = strURL;
            if (Session["User"] == null) Redirect(urlBeforeClickingHistoryOrder);

            KHACHHANG kh = (KHACHHANG)Session["User"];
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var listOrderHistory = db.DONDATHANGs.Where(p => p.MaKH == kh.MaKH).ToList();
           
            return View(listOrderHistory);
        }

        public ActionResult OrderDetail(int? orderID, decimal? totalBill)
        {
            if (Session["User"] == null) RedirectToAction("ListProduct", "Product");
            KHACHHANG kh = (KHACHHANG)Session["User"];
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var listProductInOrderDetail = db.CHITIETDONHANGs.Where(p => p.MaDonHang == orderID).ToList();  // danh sách các sản phẩm trong chi tiết đơn hàng

            List<OrderDetail> orderDetails = new List<OrderDetail>();

            foreach (var item in listProductInOrderDetail)
            {
                orderDetails.Add(new OrderDetail(item.Magiay, item.Masize,null)
                {
                    iAmount = (int)item.Soluong
                }) ;
            }

            ViewBag.TotalBill = totalBill;
            return View(orderDetails);
        }

    }
}