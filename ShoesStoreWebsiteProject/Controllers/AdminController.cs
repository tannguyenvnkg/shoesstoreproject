﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ShoesStoreWebsiteProject.Models;

using PagedList;
using PagedList.Mvc;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;

namespace LayoutAdmin.Controllers
{
    public class AdminController : Controller
    {
        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
        // GET: Admin
        // hàm lọc dấu tiếng việt
        public static string convertToUnSign(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        private bool checkLogin() // check đã có người dung đăng nhập chưa nếu chưa thì return false
        {
            QUANLY admin = Session["Admin"] as QUANLY;
            if (admin == null)
            {
                Session["Admin"] = null;
                return false;
            }
            return true;
        }
        public ActionResult _profilePartial()
        {
            var admin = (QUANLY)Session["Admin"];
            return PartialView(admin);
        }

        //=============================================DANH SÁCH SẢN PHẨM===============================================
        public ActionResult ListProduct(int? page)
        {
            if (!checkLogin()) return RedirectToAction("Login"); // session null thì bắt đăng nhập
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var listProduct = db.SANPHAMs.OrderByDescending(p => p.Magiay).ToList();
            foreach (var item in listProduct)
            {
                var supplier = db.NHACUNGCAPs.Single(p => p.MaNCC == item.MaNCC); // lấy tên nhà cung cấp
                item.tenNhaCungCap = supplier.TenNCC;
                var productType = db.LOAIGIAYs.Single(p => p.Maloai == item.Maloai); // lấy tên loại giày
                item.loaiGiay = productType.Tenloaigiay;

                var count = item.CHITIETGIAYs.Where(p => p.Magiay == item.Magiay).Sum(p => p.Soluongton); // đếm số lượng tồn
                if (count != 0) item.tinhTrangSanPham = true;
                else item.tinhTrangSanPham = false;
            }

            //===================phân trang======================
            int pageSize = 8; // mỗi trang 8 sản phẩm
            int pageNum = (page ?? 1); // nếu page = null => pageNum = 1

            return View(listProduct.ToPagedList(pageNum, pageSize));
        }
        //==============================================================================================================
        #region đăng nhập, đăng xuất
        //================================================ĐĂNG NHẬP===============================================
        [HttpGet]
        public ActionResult Login()
        {
            if (checkLogin()) return RedirectToAction("ListProduct"); // nếu có lưu session đăng nhập thì vào luôn web
            return View();
        }

        [HttpPost]
        public ActionResult Login(FormCollection collection)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var username = collection["username"];
            var password = collection["password"];
            var admin = db.QUANLies.SingleOrDefault(p => p.Taikhoan == username);
            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
            {
                ViewData["Error"] = "Vui Lòng Điền Đầy Đủ Nội Dung";
                return this.Login();
            }
            else if (admin == null)
            {
                ViewData["Error"] = "Sai Tài Khoản";
                return this.Login();
            }
            else if (!String.Equals(MD5Hash(password), admin.Matkhau))
            {
                ViewData["Error"] = "Sai Mật Khẩu";
                return this.Login();
            }
            else if (!admin.Trangthai)
            {
                ViewData["Error"] = "Tài Khoản Đang Bị Khóa";
                return this.Login();
            }
            else
            {
                Session["Admin"] = admin;
                return RedirectToAction("ListProduct");
            }
        }
        //================================================ĐĂNG XUẤT===============================================
        public ActionResult LogOut()
        {
            Session["Admin"] = null;
            return RedirectToAction("Login");
        }
        //==================================================================================================================
        #endregion

        #region thêm sản phẩm
        //===================================THÊM SẢN PHẨM==================================================
        [HttpGet]
        public ActionResult AddProduct()
        {
            if (!checkLogin()) return RedirectToAction("Login"); // session null thì bắt đăng nhập
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            SANPHAM product = new SANPHAM(); // khởi tạo 1 sản phẩm
            var listSupplier = db.NHACUNGCAPs.ToList(); //nhà cung cấp - supplier
            var listShoesType = db.LOAIGIAYs.ToList();
            //============== gán 2 list vào, các giá trị khác cứ để trống=====================
            product.listnhaCungCap = listSupplier;
            product.listloaiGiay = listShoesType;

            return View(product);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddProduct(FormCollection collection, HttpPostedFileBase fileUpload)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var name = collection["name"];
            var price = collection["price"];
            //var imageCover = collection["imageCover"];
            var supplierID = Int32.Parse(collection["supplier"]);
            var shoesTypeID = Int32.Parse(collection["shoesType"]);

            if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(price))
            {
                ViewData["Error"] = "Vui lòng điền đầy đủ thông tin";
                return this.AddProduct();
            }
            if (fileUpload == null)
            {
                ViewData["Error"] = "Vui lòng chọn ảnh bìa";
                return this.AddProduct();
            }
            //===============================thêm ảnh vào thư mục=============================================
            var imageCover = Path.GetFileName(fileUpload.FileName);
            // lưu đường dẫn
            var path = Path.Combine(Server.MapPath("~/Content/ProductImages"), imageCover);
            // kiểm tra hình đã tồn tại hay chưa
            //if (System.IO.File.Exists(path))
            //{
            //    ViewData["Error"] = "Hình ảnh đã tồn tại";
            //    return this.AddProduct();
            //}
            //else
            //{
            //    fileUpload.SaveAs(path); // lưu ảnh vào đường dẫn
            //}
            if (!System.IO.File.Exists(path))
            {
                fileUpload.SaveAs(path); // lưu ảnh vào đường dẫn
            }
            //================================================================================================
            //===============================thêm vào table sản phẩm==========================================
            var product = new SANPHAM();
            product.Tengiay = name;
            product.Giaban = decimal.Parse(price);
            product.Anhbia = imageCover;
            product.Ngaycapnhat = DateTime.Now;
            product.MaNCC = supplierID;
            product.Maloai = shoesTypeID;
            product.Trangthai = true;
            db.SANPHAMs.Add(product);
            db.SaveChanges();
            //================================================================================================
            //===============================thêm vào table chi tiết giày=====================================
            List<CHITIETGIAY> listProductDetail = new List<CHITIETGIAY>();
            for (int i = 0; i < 7; i++)
            {
                listProductDetail.Add(new CHITIETGIAY()
                {
                    Magiay = product.Magiay,
                    Masize = i + 1,
                    Soluongton = 0
                });
            }
            foreach (var item in listProductDetail)
            {
                db.CHITIETGIAYs.Add(item);
            }
            db.SaveChanges();
            //================================================================================================
            ViewData["Success"] = "Thêm Sản Phẩm Thành Công";
            return this.AddProduct();
        }
        //======================================================================================================
        #endregion

        #region cập nhật sản phẩm
        //=========================================CẬP NHẬT THÔNG TIN SẢN PHẨM======================================
        //public string urlBackToList;
        [HttpGet]
        public ActionResult EditProduct(int id, string url)
        {
            if (!checkLogin()) return RedirectToAction("Login"); // session null thì bắt đăng nhập
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var product = db.SANPHAMs.SingleOrDefault(p => p.Magiay == id);
            //===========================gán list ncc và loại giày==============================
            var listSupplier = db.NHACUNGCAPs.ToList(); //nhà cung cấp - supplier
            var listShoesType = db.LOAIGIAYs.ToList();
            //============== gán 2 list vào, các giá trị khác cứ để trống=====================
            product.listnhaCungCap = listSupplier;
            product.listloaiGiay = listShoesType;
            if (!String.IsNullOrEmpty(url)) product.urlBack = url;
            return View(product);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditProduct(int id, HttpPostedFileBase fileUpload, FormCollection collection)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var name = collection["name"];
            var price = collection["price"];
            var imageCover = "";
            var url = collection["url"];
            //var imageCover = collection["imageCover"];
            var supplierID = Int32.Parse(collection["supplier"]);
            var shoesTypeID = Int32.Parse(collection["shoesType"]);

            if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(price))
            {
                ViewData["Error"] = "Vui lòng điền đầy đủ thông tin";
                return this.EditProduct(id, url);
            }
            if (fileUpload != null)
            {
                //===============================thêm ảnh vào thư mục=====================================================
                imageCover = Path.GetFileName(fileUpload.FileName);
                // lưu đường dẫn
                var path = Path.Combine(Server.MapPath("~/Content/ProductImages"), imageCover);
                if (!System.IO.File.Exists(path)) // sản phẩm chưa tồn tại
                {
                    fileUpload.SaveAs(path); // lưu ảnh vào đường dẫn
                }
                //========================================================================================================
            }
            //===============================thêm vào table sản phẩm======================================================
            var product = db.SANPHAMs.Single(p => p.Magiay == id);
            product.Tengiay = name;
            product.Giaban = decimal.Parse(price);
            if (!String.IsNullOrEmpty(imageCover)) product.Anhbia = imageCover; // nếu có cập nhật ảnh thì chạy dòng này
            product.Ngaycapnhat = DateTime.Now;
            product.MaNCC = supplierID;
            product.Maloai = shoesTypeID;
            db.SaveChanges();
            //============================================================================================================
            ViewData["Success"] = "Sửa Sản Phẩm Thành Công";
            return this.EditProduct(id, url);
        }
        //================================================================================================================

        //=========================================CẬP NHẬT SỐ LƯỢNG SẢN PHẨM=============================================
        [HttpGet]
        public ActionResult UpdateProduct(int id, string url)
        {
            if (!checkLogin()) return RedirectToAction("Login"); // session null thì bắt đăng nhập
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var product = db.SANPHAMs.Single(p => p.Magiay == id);
            var maSize = db.CHITIETGIAYs.Where(p => p.Magiay == id).Select(p => p.Masize).ToList();
            var soLuongTon = db.CHITIETGIAYs.Where(p => p.Magiay == id).Select(p => p.Soluongton).ToList();
            var sizegiay = db.SIZEGIAYs.Select(p => p.Sosize).ToList();

            product.maSize = maSize;
            product.soluongton = soLuongTon;
            product.sizeGiay = sizegiay;

            if (!String.IsNullOrEmpty(url)) product.urlBack = url; // gán url để trở lại list
            return View(product);
        }

        [HttpPost]
        public ActionResult UpdateProduct(FormCollection collection, int id)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var size36 = collection["size36"];
            var size37 = collection["size37"];
            var size38 = collection["size38"];
            var size39 = collection["size39"];
            var size40 = collection["size40"];
            var size41 = collection["size41"];
            var size42 = collection["size42"];
            var url = collection["url"];

            if (String.IsNullOrEmpty(size36) || String.IsNullOrEmpty(size37) || String.IsNullOrEmpty(size38) ||
               String.IsNullOrEmpty(size39) || String.IsNullOrEmpty(size40) || String.IsNullOrEmpty(size41) ||
               String.IsNullOrEmpty(size42))
            {
                ViewData["Error"] = "Vui Lòng Điền Đầy Đủ Size Giày";
                return this.UpdateProduct(id, url);
            }

            var productSize36 = db.CHITIETGIAYs.Single(p => p.Magiay == id && p.Masize == 1);
            productSize36.Soluongton = Int32.Parse(size36);
            var productSize37 = db.CHITIETGIAYs.Single(p => p.Magiay == id && p.Masize == 2);
            productSize37.Soluongton = Int32.Parse(size37);
            var productSize38 = db.CHITIETGIAYs.Single(p => p.Magiay == id && p.Masize == 3);
            productSize38.Soluongton = Int32.Parse(size38);
            var productSize39 = db.CHITIETGIAYs.Single(p => p.Magiay == id && p.Masize == 4);
            productSize39.Soluongton = Int32.Parse(size39);
            var productSize40 = db.CHITIETGIAYs.Single(p => p.Magiay == id && p.Masize == 5);
            productSize40.Soluongton = Int32.Parse(size40);
            var productSize41 = db.CHITIETGIAYs.Single(p => p.Magiay == id && p.Masize == 6);
            productSize41.Soluongton = Int32.Parse(size41);
            var productSize42 = db.CHITIETGIAYs.Single(p => p.Magiay == id && p.Masize == 7);
            productSize42.Soluongton = Int32.Parse(size42);

            var product = db.SANPHAMs.Single(p => p.Magiay == id);
            product.Ngaycapnhat = DateTime.Now;
            db.SaveChanges();
            ViewData["Success"] = "Cập Nhật Số Lượng Thành Công";
            return this.UpdateProduct(id, url);
        }
        //================================================================================================================
        #endregion

        #region Đơn hàng
        //===================================================ĐƠN HÀNG===================================================

        //==================danh sách đơn hàng=============================
        public ActionResult ListOrder(int? page)
        {
            if (!checkLogin()) return RedirectToAction("Login");
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var listOrder = db.DONDATHANGs.ToList();

            //===================phân trang======================
            int pageSize = 10; // mỗi trang 10 đơn hàng
            int pageNum = (page ?? 1); // nếu page = null => pageNum = 1

            return View(listOrder.ToPagedList(pageNum, pageSize));
        }

        //=================chi tiết đơn hàng===============================
        public ActionResult OrderDetail(int? orderID, decimal? totalBill, int? userID, string strURL)
        {
            if (!checkLogin()) return RedirectToAction("Login");
            KHACHHANG kh = (KHACHHANG)Session["User"];
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var listProductInOrderDetail = db.CHITIETDONHANGs.Where(p => p.MaDonHang == orderID).ToList();  // danh sách các sản phẩm trong chi tiết đơn hàng

            List<OrderDetail> orderDetails = new List<OrderDetail>();

            foreach (var item in listProductInOrderDetail)
            {
                orderDetails.Add(new OrderDetail(item.Magiay, item.Masize, orderID)
                {
                    iAmount = (int)item.Soluong,
                    // iUserID = userID,
                    urlBackToListOrder = strURL
                });
            }

            var user = db.KHACHHANGs.SingleOrDefault(p => p.MaKH == userID); // lấy thông tin người dùng gắn vào viewbag
            ViewBag.userID = user.MaKH;
            ViewBag.userName = user.HoTen;
            ViewBag.TotalBill = totalBill;
            ViewBag.orderID = orderID;
            return View(orderDetails);
        }
        //==============================================================================================================
        #endregion

        #region khách hàng
        //============================================KHÁCH HÀNG========================================================
        //========================danh sách khách hảng============================
        public ActionResult ListUser(int? page)
        {
            if (!checkLogin()) return RedirectToAction("Login");
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var listUser = db.KHACHHANGs.ToList();

            //===================phân trang======================
            int pageSize = 10; // mỗi trang 8 user
            int pageNum = (page ?? 1); // nếu page = null => pageNum = 1

            return View(listUser.ToPagedList(pageNum, pageSize));
        }
        //========================thông tin khách hảng============================
        public ActionResult UserInformation(int id, string url)
        {
            if (!checkLogin()) return RedirectToAction("Login");
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var user = db.KHACHHANGs.Single(p => p.MaKH == id);
            user.urlBack = url;
            return View(user);
        }

        //========================lịch sử đặt hàng của khách hàng=================
        public ActionResult UserOrderHistory(int id, string userName, string strURL, int? page)
        {
            if (!checkLogin()) return RedirectToAction("Login");
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var listOrder = db.DONDATHANGs.Where(p => p.MaKH == id).ToList();
            if (listOrder.Count != 0) listOrder.First().urlBacktoUserInformation = strURL;
            else ViewBag.urlBacktoUserInformation = strURL;
            ViewBag.userName = userName;

            //===================phân trang======================
            int pageSize = 10; // mỗi trang 10 đơn hàng
            int pageNum = (page ?? 1); // nếu page = null => pageNum = 1

            return View(listOrder.ToPagedList(pageNum, pageSize));
           // return View(listOrder);
        }
        //==============================================================================================================
        #endregion

        #region danh sách admin
        //=================================================DANH SÁCH ADMIN========================================================
        public ActionResult ListAdmin(int? page)
        {
            if (!checkLogin()) return RedirectToAction("Login");
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var listAdmin = db.QUANLies.ToList();

            //===================phân trang======================
            int pageSize = 10; // mỗi trang 8 user
            int pageNum = (page ?? 1); // nếu page = null => pageNum = 1

            return View(listAdmin.ToPagedList(pageNum, pageSize));
        }
        //==============================================================================================================
        #endregion

        #region ý kiến khách hàng
        //=============================================Ý KIẾN KHÁCH HÀNG===============================================
        //==========danh sách ý kiến================
        public ActionResult GuestComment(int? page)
        {
            if (!checkLogin()) return RedirectToAction("Login"); // session null thì bắt đăng nhập
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var listGuestComment = db.YKienKhachHangs.OrderBy(p=>p.TrangThai).ToList();

            //===================phân trang======================
            int pageSize = 8; // mỗi trang 8 sản phẩm
            int pageNum = (page ?? 1); // nếu page = null => pageNum = 1

            return View(listGuestComment.ToPagedList(pageNum, pageSize));
        }

        //============chi tiết ý kiến=================
        public ActionResult CommentDetail(int id, string url)
        {
            if (!checkLogin()) return RedirectToAction("Login"); // session null thì bắt đăng nhập
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var commentDetail = db.YKienKhachHangs.Where(p => p.MaYKien == id).Single();
            commentDetail.urlBack = url;
            commentDetail.TrangThai = true; // bật trạng thái đã đọc
            db.SaveChanges();

            return View(commentDetail);
        }
        //==============================================================================================================
        #endregion

        #region tìm kiếm sản phẩm
        //=============================tìm kiếm sản phẩm============================
        public ActionResult SearchProduct(int? page, string value, FormCollection f)
        {
            if (!checkLogin()) return RedirectToAction("Login"); // session null thì bắt đăng nhập
            var search = f["search"];
            string searchingValue = "";
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            List<SANPHAM> listProduct = null;
            if (!String.IsNullOrEmpty(search))
            {
                listProduct = db.SANPHAMs.Where(p => p.Tengiay.Contains(search)).ToList();// nếu người dùng nhập vào ô tìm kiếm thì listProduct sẽ xuất giá trị theo biến search 
                searchingValue = search;
            }
            else
            {
                listProduct = db.SANPHAMs.Where(p => p.Tengiay.Contains(value)).ToList(); // giá trị sẽ chạy khi người dùng đã nhập vào ô tìm kiếm sản phẩm và click sang những trang tiếp theo
                searchingValue = value;
            }

            foreach (var item in listProduct)
            {
                var supplier = db.NHACUNGCAPs.Single(p => p.MaNCC == item.MaNCC); // lấy tên nhà cung cấp
                item.tenNhaCungCap = supplier.TenNCC;
                var productType = db.LOAIGIAYs.Single(p => p.Maloai == item.Maloai); // lấy tên loại giày
                item.loaiGiay = productType.Tenloaigiay;

                var count = item.CHITIETGIAYs.Where(p => p.Magiay == item.Magiay).Sum(p => p.Soluongton); // đếm số lượng tồn
                if (count != 0) item.tinhTrangSanPham = true;
                else item.tinhTrangSanPham = false;

                item.searchingValue = searchingValue; // lưu giá trị này để người dùng truy cập vào những trang tiếp theo sẽ hiện
            }
            //===================phân trang======================
            int pageSize = 8; // mỗi trang 8 sản phẩm
            int pageNum = (page ?? 1); // nếu page = null => pageNum = 1

            return View(listProduct.ToPagedList(pageNum, pageSize));
        }
        //=======================================================================================================
        #endregion

        #region danh sách nhà cung cấp
        //================================================Danh sách nhà cung cấp===============================================
        public ActionResult ListNCC(int? page)
        {
            if (!checkLogin()) return RedirectToAction("Login"); // session null thì bắt đăng nhập
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var listncc = db.NHACUNGCAPs.OrderByDescending(p => p.MaNCC).ToList();

            //===================phân trang======================
            int pageSize = 8; // mỗi trang 8 sản phẩm
            int pageNum = (page ?? 1); // nếu page = null => pageNum = 1

            return View(listncc.ToPagedList(pageNum, pageSize));
        }
        //=======================================================================================================
        #endregion

        #region thêm nhà cung cấp
        [HttpGet]
        public ActionResult AddNCC()
        {
            if (!checkLogin()) return RedirectToAction("Login"); // session null thì bắt đăng nhập
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            NHACUNGCAP ncc = new NHACUNGCAP(); // khởi tạo 1 nhà cung cấp

            return View(ncc);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddNCC(FormCollection collection)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var name = collection["name"];
            var address = collection["address"];
            var phone = collection["phone"];

            if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(address) || String.IsNullOrEmpty(phone))
            {
                ViewData["Error"] = "Vui lòng điền đầy đủ thông tin";
                return this.AddNCC();
            }

            var ncc = new NHACUNGCAP();
            ncc.TenNCC = name;
            ncc.Diachi = address;
            ncc.DienThoai = phone;
            ncc.Trangthai = true;
            db.NHACUNGCAPs.Add(ncc);
            db.SaveChanges();
            ViewData["Success"] = "Thêm Nhà Cung Cấp Thành Công";
            return this.AddNCC();
        }
        #endregion

        #region cập nhật nhà cung cấp
        [HttpGet]
        public ActionResult EditNCC(int id)
        {
            if (!checkLogin()) return RedirectToAction("Login"); // session null thì bắt đăng nhập
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var ncc = db.NHACUNGCAPs.SingleOrDefault(p => p.MaNCC == id);
         
            return View(ncc);
        }

        public ActionResult EditNCC(int id, FormCollection collection)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var name = collection["name"];
            var address = collection["address"];
            var phone = collection["phone"];

            if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(address) || String.IsNullOrEmpty(phone))
            {
                ViewData["Error"] = "Vui lòng điền đầy đủ thông tin";
                return this.EditNCC(id);
            }

            var ncc = db.NHACUNGCAPs.Single(p => p.MaNCC == id);
            ncc.TenNCC = name;
            ncc.Diachi = address;
            ncc.DienThoai = phone;
            db.SaveChanges();

            ViewData["Success"] = "Sửa Sản Phẩm Thành Công";
            return this.EditNCC(id);
        }

        #endregion

        #region danh loại sản phẩm
        //================================================Danh sách loại sản phẩm===============================================
        public ActionResult ListloaiSP(int? page)
        {
            if (!checkLogin()) return RedirectToAction("Login"); // session null thì bắt đăng nhập
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var listloaiSP = db.LOAIGIAYs.OrderByDescending(p => p.Maloai).ToList();

            //===================phân trang======================
            int pageSize = 8; // mỗi trang 8 sản phẩm
            int pageNum = (page ?? 1); // nếu page = null => pageNum = 1

            return View(listloaiSP.ToPagedList(pageNum, pageSize));
        }
        //=======================================================================================================
        #endregion

        #region thêm loại sản phẩm
        [HttpGet]
        public ActionResult AddLoaiSP()
        {
            if (!checkLogin()) return RedirectToAction("Login"); // session null thì bắt đăng nhập
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            LOAIGIAY loaisp = new LOAIGIAY(); // khởi tạo 1 loại sản phẩm

            return View(loaisp);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddLoaiSP(FormCollection collection)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var name = collection["name"];

            if (String.IsNullOrEmpty(name))
            {
                ViewData["Error"] = "Vui lòng điền đầy đủ thông tin";
                return this.AddLoaiSP();
            }

            var loaisp = new LOAIGIAY();
            loaisp.Tenloaigiay = name;
            loaisp.Trangthai = true;
            db.LOAIGIAYs.Add(loaisp);
            db.SaveChanges();
            ViewData["Success"] = "Thêm Nhà Cung Cấp Thành Công";
            return this.AddLoaiSP();
        }
        #endregion

        #region cập nhật loại sản phẩm
        [HttpGet]
        public ActionResult EditLoaiSP(int id)
        {
            if (!checkLogin()) return RedirectToAction("Login"); // session null thì bắt đăng nhập
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var loaisp = db.LOAIGIAYs.SingleOrDefault(p => p.Maloai == id);

            return View(loaisp);
        }

        public ActionResult EditLoaiSP(int id, FormCollection collection)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var name = collection["name"];
          
            if (String.IsNullOrEmpty(name))
            {
                ViewData["Error"] = "Vui lòng điền đầy đủ thông tin";
                return this.EditLoaiSP(id);
            }

            var loaisp = db.LOAIGIAYs.Single(p => p.Maloai == id);
            loaisp.Tenloaigiay = name;
            db.SaveChanges();

            ViewData["Success"] = "Sửa Sản Phẩm Thành Công";
            return this.EditLoaiSP(id);
        }

        #endregion

        #region Danh sách sản phẩm hết hàng
        //=============================================DANH SÁCH SẢN PHẨM HẾT HÀNG===============================================
        public ActionResult ListOutOfProduct(int? page)
        {
            if (!checkLogin()) return RedirectToAction("Login"); // session null thì bắt đăng nhập
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var listProduct = db.SANPHAMs.OrderByDescending(p => p.Magiay).ToList();
            List<SANPHAM> listOutOfProduct = new List<SANPHAM>();
            foreach (var item in listProduct)
            {
                var supplier = db.NHACUNGCAPs.Single(p => p.MaNCC == item.MaNCC); // lấy tên nhà cung cấp
                item.tenNhaCungCap = supplier.TenNCC;
                var productType = db.LOAIGIAYs.Single(p => p.Maloai == item.Maloai); // lấy tên loại giày
                item.loaiGiay = productType.Tenloaigiay;

                var count = item.CHITIETGIAYs.Where(p => p.Magiay == item.Magiay).Sum(p => p.Soluongton); // đếm số lượng tồn
                if (count != 0) item.tinhTrangSanPham = true;
                else 
                {
                    item.tinhTrangSanPham = false;
                    listOutOfProduct.Add(item);
                }
            }

            //===================phân trang======================
            int pageSize = 8; // mỗi trang 8 sản phẩm
            int pageNum = (page ?? 1); // nếu page = null => pageNum = 1

            return View(listOutOfProduct.ToPagedList(pageNum, pageSize));
        }
        //==============================================================================================================
        #endregion

        #region Danh sách sản phẩm hết hàng
        //=============================================DANH SÁCH SẢN PHẨM NGỪNG KINH DOANH===============================================
        public ActionResult ListNotSellProduct(int? page) //List Not Sell Product = sản phẩm ngừng kinh doanh
        {
            if (!checkLogin()) return RedirectToAction("Login"); // session null thì bắt đăng nhập
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var listProduct = db.SANPHAMs.OrderByDescending(p => p.Magiay).ToList();
            List<SANPHAM> listNotSellProduct = new List<SANPHAM>();
            foreach (var item in listProduct)
            {
                var supplier = db.NHACUNGCAPs.Single(p => p.MaNCC == item.MaNCC); // lấy tên nhà cung cấp
                item.tenNhaCungCap = supplier.TenNCC;
                var productType = db.LOAIGIAYs.Single(p => p.Maloai == item.Maloai); // lấy tên loại giày
                item.loaiGiay = productType.Tenloaigiay;
                if(item.Trangthai == false) 
                {
                    listNotSellProduct.Add(item);
                }
            }

            //===================phân trang======================
            int pageSize = 8; // mỗi trang 8 sản phẩm
            int pageNum = (page ?? 1); // nếu page = null => pageNum = 1

            return View(listNotSellProduct.ToPagedList(pageNum, pageSize));
        }
        //==============================================================================================================
        #endregion


        #region JSON
        //=============================================HÀM JSON=======================================================
        // tạo tài khoản admin
        public JsonResult CreateAdmin(string newAdmin)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            QUANLY admin = serializer.Deserialize<QUANLY>(newAdmin);
            bool Status = false;
            string errMessage = string.Empty;
            admin.Taikhoan = convertToUnSign(admin.Taikhoan);
            var exitAdmin = db.QUANLies.SingleOrDefault(p => p.Taikhoan == admin.Taikhoan);

            if (exitAdmin != null)
                return Json(new // trả về lỗi tồn tại tài khoản
                {
                    status = Status, 
                    exit = "Tài khoản đã tồn tại"
                });
            else
            {
                admin.Matkhau = MD5Hash(admin.Matkhau);
                admin.Chusohuu = false;
                admin.Trangthai = true;
                db.QUANLies.Add(admin);
                try
                {
                    db.SaveChanges();
                    Status = true; 
                }
                catch (Exception ex)
                {
                    Status = false;
                    errMessage = ex.Message;
                }
            }

            return Json(new
            {
                status = Status,
                errorMessage = errMessage
            });
        }

        // thay đổi mật khẩu admin
        public JsonResult ChangePasswordAdmin(string id, string password)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var admin = db.QUANLies.Single(p => p.Taikhoan.Equals(id));
            bool Status = false;
            string errMessage = string.Empty;

            if ((bool)admin.Chusohuu)
                return Json(new // trả về lỗi tồn tại tài khoản
                {
                    status = Status,
                    exit = "Không thể thay đổi mật khẩu tài khoản của chủ sở hữu"
                });
            else
            {
                admin.Matkhau = MD5Hash(password);
                try
                {
                    db.SaveChanges();
                    Status = true;
                }
                catch (Exception ex)
                {
                    Status = false;
                    errMessage = ex.Message;
                }
            }

            return Json(new
            {
                status = Status,
                errorMessage = errMessage,
            });
        }
        // khóa hoặc mở khóa tài khoản admin
        public JsonResult LockOrUnlockAdmin(string id)
        {
            bool lockOrUnlock = true; // khóa hay mở khóa tài khoản (true: khóa, false: mở khóa)
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var admin = db.QUANLies.Single(p => p.Taikhoan.Equals(id));
            bool Status = false;
            string err = string.Empty;
            if (admin != null)
            {
                if ((bool)admin.Chusohuu) // check chủ sỡ hữu
                    return Json(new
                    {
                        status = true,
                        exit = "Không thể khóa chủ sở hữu"
                    });
                if (admin.Trangthai) admin.Trangthai = false; // nếu đang hoạt động thì khóa
                else
                {
                    admin.Trangthai = true;
                    lockOrUnlock = false; // mở khóa tài khoản
                }

                try
                {
                    db.SaveChanges();
                    Status = true;
                }
                catch (Exception ex)
                {
                    err = ex.Message;
                }
            }

            return Json(new
            {
                status = Status,
                errorMessage = err,
                lockStatus = lockOrUnlock
            });

        }
        // ngừng kinh doanh sản phẩm
        public JsonResult StopSellingOrSelling(string id)
        {
            bool stopSellingOrSelling = true; // kinh doanh hoặc ngừng kinh doanh tài khoản (true: ngừng kinh doanh, false: kinh doanh)
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            int shoesID = Int32.Parse(id);
            var product = db.SANPHAMs.Single(p => p.Magiay == shoesID);
            bool Status = false;
            string err = string.Empty;
            if (product != null)
            {
                if ((bool)product.Trangthai) product.Trangthai = false; // nếu đang kinh doanh thì ngừng kinh doanh
                else
                {
                    product.Trangthai = true;
                    stopSellingOrSelling = false; // mở kinh doanh sản phẩm
                }

                try
                {
                    db.SaveChanges();
                    Status = true;
                }
                catch (Exception ex)
                {
                    err = ex.Message;
                }
            }
            return Json(new
            {
                status = Status,
                errorMessage = err,
                sellingStatus = stopSellingOrSelling
            });
        }
        // thay đổi mật khẩu admin đăng nhập
        public JsonResult changePasswordAdminLogin(string id, string oldPassword, string newPassword)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var admin = db.QUANLies.Single(p => p.Taikhoan.Equals(id));
            bool Status = false;
            string errMessage = string.Empty;
            string successMessage = string.Empty;
            if (!admin.Matkhau.Equals(MD5Hash(oldPassword)))
                return Json(new
                {
                    status = Status,
                    errorMessage = "Sai mật khẩu cũ!!"
                });
            else
            {
                admin.Matkhau = MD5Hash(newPassword);
                try
                {
                    db.SaveChanges();
                    Status = true;
                    successMessage = "Đổi mật khẩu thành công";
                    var a = (QUANLY)Session["Admin"]; // cập nhật lại mật khẩu cho session đăng nhập
                    a.Matkhau = newPassword;
                }
                catch (Exception ex)
                {
                    Status = false;
                    errMessage = ex.Message;
                }
            }

            return Json(new
            {
                status = Status,
                errorMessage = errMessage,
                successMessage = successMessage
            });
        }

        // hoàn thành đơn hàng
        public JsonResult CompleteOrder(string id)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            int orderID = Int32.Parse(id);
            var order = db.DONDATHANGs.Single(p => p.MaDonHang == orderID);

            bool Status = false;
            string err = string.Empty;
            string successMessage = string.Empty;
            if (order != null)
            {

                if ((bool)order.Tinhtranggiaohang) // nếu đơn hàng vốn đã hoàn thành rồi thì return lỗi
                    err = "Đơn hàng này đã hoàn thành rồi!!!";
                else
                {
                    order.Tinhtranggiaohang = true;
                    order.Ngaygiao = DateTime.Now;
                    order.Dathanhtoan = true;
                    try
                    {
                        db.SaveChanges();
                        Status = true;
                        successMessage = "hoàn đơn hàng thành công";
                    }
                    catch (Exception ex)
                    {
                        err = ex.Message;
                    }
                }
            }

            return Json(new
            {
                status = Status,
                errorMessage = err,
                successMessage = successMessage
            });

        }

        public JsonResult HideOrUnhideCategory(string id) // Ẩn, Mở loại sản phẩm
        {
            bool hideOrUnhideCategory = true; // Mở thể loại hoặc ẩn thể loại (true: ẩn, false: mở)
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            int categoryID = Int32.Parse(id);
            var category = db.LOAIGIAYs.Single(p => p.Maloai == categoryID);
            bool Status = false;
            string err = string.Empty;
            if (category != null)
            {
                if ((bool)category.Trangthai) category.Trangthai = false; // nếu đang kinh doanh thì ngừng kinh doanh
                else
                {
                    category.Trangthai = true;
                    hideOrUnhideCategory = false; // mở kinh doanh sản phẩm
                }

                try
                {
                    db.SaveChanges();
                    Status = true;
                }
                catch (Exception ex)
                {
                    err = ex.Message;
                }
            }
            return Json(new
            {
                status = Status,
                errorMessage = err,
                sellingStatus = hideOrUnhideCategory
            });
        }

        public JsonResult HideOrUnhideNCC(string id)
        {
            bool hideOrUnhideNCC = true; // Mở thể loại hoặc ẩn thể loại (true: ẩn, false: mở)
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            int NCCID = Int32.Parse(id);
            var NCC = db.NHACUNGCAPs.Single(p => p.MaNCC == NCCID);
            bool Status = false;
            string err = string.Empty;
            if (NCC != null)
            {
                if ((bool)NCC.Trangthai) NCC.Trangthai = false; // nếu đang kinh doanh thì ngừng kinh doanh
                else
                {
                    NCC.Trangthai = true;
                    hideOrUnhideNCC = false; // mở kinh doanh sản phẩm
                }

                try
                {
                    db.SaveChanges();
                    Status = true;
                }
                catch (Exception ex)
                {
                    err = ex.Message;
                }
            }
            return Json(new
            {
                status = Status,
                errorMessage = err,
                sellingStatus = hideOrUnhideNCC
            });
        }
        #endregion
    }
}
