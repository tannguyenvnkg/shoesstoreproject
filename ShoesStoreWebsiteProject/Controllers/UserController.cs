﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ShoesStoreWebsiteProject.Models;
namespace WebBanGiay.Controllers
{
    public class UserController : Controller
    {
        private static string urlAfterLogin; // lưu lại link đang ở trước khi nhấn đăng nhập
        private static readonly int CHECK_USERNAME = 1;
        private static readonly int CHECK_EMAIL = 2;

        #region gửi mail
        public static void SendEmail(string address, string subject, string message)
        {
            if (new EmailAddressAttribute().IsValid(address)) // check có đúng mail khách hàng
            {
                string email = "inazumaqiqi@gmail.com";
                var senderEmail = new MailAddress(email, "Teacup Shoes Store (tin nhắn tự động)");
                var receiverEmail = new MailAddress(address, "Receiver");
                var password = "fxjfpulvdnahfmpj";
                var sub = subject;
                var body = message;
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(senderEmail.Address, password)
                };
                using (var mess = new MailMessage(senderEmail, receiverEmail)
                {
                    Subject = sub,
                    Body = body
                })
                {
                    smtp.Send(mess);
                }
            }
        }
        #endregion

        #region mã hóa MD5
        public static string MD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
        #endregion

        #region đăng nhập
        [HttpGet]
        public ActionResult Login(string strURL)
        {
            // kiểm tra đường dẫn có null không, tránh tình trạng paste trực tiếp đường link vào url sẽ không lưu được trả về trang chủ
            if (!String.IsNullOrEmpty(strURL)) urlAfterLogin = strURL;
            else RedirectToAction("Home", "Home");

            //if (Session["User"] != null) return RedirectToAction("ListProduct", "Product");
            return View();
        }

        [HttpPost]
        public ActionResult Login(FormCollection collection)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var username = collection["username"];
            var password = collection["password"];
            var user = db.KHACHHANGs.SingleOrDefault(p => p.Taikhoan == username);
            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
            {
                ViewData["Error"] = "Vui lòng điền đầy đủ nội dung";
                return this.Login(urlAfterLogin);
            }
            else if (user == null)
            {
                ViewData["Error"] = "Sai tài khoản";
                return this.Login(urlAfterLogin);
            }
            else if (!String.Equals(MD5Hash(password), user.Matkhau))
            {
                ViewData["Error"] = "Sai mật khẩu";
                return this.Login(urlAfterLogin);
            }
            else
            {
                Session["User"] = user;
                //return RedirectToAction("ListProduct", "Product");
                return Redirect(urlAfterLogin);
            }
        }
        #endregion

        #region đăng ký
        [HttpGet]
        public ActionResult Signup()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Signup(FormCollection collection)
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var username = collection["username"];
            var password = collection["password"];
            var confirmPassword = collection["confirmPassword"];
            var email = collection["email"];
            var address = collection["address"];
            var phoneNumber = collection["phoneNumber"];

            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password) ||
                String.IsNullOrEmpty(confirmPassword) || String.IsNullOrEmpty(email) ||
                String.IsNullOrEmpty(address) || String.IsNullOrEmpty(phoneNumber))
            {
                ViewData["Error"] = "Vui lòng điền đầy đủ nội dung";
                return this.Signup();
            }
            else if (checkUser(username,CHECK_USERNAME))  // kiểm tra tài khoản đã tồn tại hay chưa
            {
                ViewData["Error"] = "Tài khoản đã tồn tại";
                return this.Signup();
            }
            else if (checkUser(email,CHECK_EMAIL))  // kiểm tra tài khoản đã tồn tại hay chưa
            {
                ViewData["Error"] = "Email đã tồn tại";
                return this.Signup();
            }
            else if (!String.Equals(password.ToString(), confirmPassword.ToString()))
            {
                ViewData["Error"] = "Mật khẩu không khớp";
                return this.Signup();
            }
            else
            {
                KHACHHANG kh = new KHACHHANG()
                {
                    Taikhoan = username,
                    HoTen = username,
                    Matkhau = MD5Hash(password),
                    Email = email,
                    DiachiKH = address,
                    DienthoaiKH = phoneNumber,
                    Ngaysinh = null
                };
                db.KHACHHANGs.Add(kh);
                db.SaveChanges();
                string subject = "Chào Mừng Đến Với TeaCup Shoes Store";
                string message = "Chào mừng " + kh.HoTen +" đến với TeaCup Shoes Store, Chúc bạn có một ngày vui vẻ.";
                SendEmail(email, subject, message); // gửi mail chào mừng
                return RedirectToAction("Login");
            }
        }
        #endregion

        private Boolean checkUser(string str, int value) // nếu tồn tại tài khoản hay email thì trả về true
        {
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            if(value == 1)
            {
                var a = db.KHACHHANGs.FirstOrDefault(p => p.Taikhoan == str);
                if (a != null) return true;
            }
            else if (value == 2)
            {
                var a = db.KHACHHANGs.FirstOrDefault(p => p.Email == str);
                if (a != null) return true;
            }
            return false;
        }

        public ActionResult _User() // hiện tên user hoặc username trên thanh navigation
        {
            return PartialView();
        }

        public ActionResult LogOut() // đăng xuất
        {
            Session["User"] = null;
            urlAfterLogin = null;
            return RedirectToAction("ListProduct", "Product");
        }

        [HttpGet]
        public ActionResult MyProfile() // chuyển đến trang hồ sơ
        {
            if (Session["User"] == null) RedirectToAction("Login");
            KHACHHANG kh = (KHACHHANG)Session["User"];
            
            if (kh.Ngaysinh != null)
            {
                DateTime date = (DateTime)kh.Ngaysinh;
                kh.Day = date.Day;
                kh.Month = date.Month;
                kh.Year = date.Year;
            }
            return View(kh);
        }

        [HttpPost]
        public ActionResult MyProfile(string strURL, FormCollection collection)
        {
            if (Session["User"] == null) RedirectToAction("Login");
            KHACHHANG kh = (KHACHHANG)Session["User"];
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var user = db.KHACHHANGs.SingleOrDefault(p => p.MaKH == kh.MaKH);

            var name = collection["name"];
            var email = collection["email"];
            var address = collection["address"];
            var phoneNumber = collection["phoneNumber"];
            var day = collection["day"];
            var month = collection["month"];
            var year = collection["year"];

            if (String.IsNullOrEmpty(name) || String.IsNullOrEmpty(email) || // kiểm tra null
                String.IsNullOrEmpty(address) || String.IsNullOrEmpty(phoneNumber) ||
                String.IsNullOrEmpty(day) || String.IsNullOrEmpty(month) ||
                String.IsNullOrEmpty(year))
            {
                ViewData["Error"] = "Vui lòng điền đủ thông tin";
                return this.MyProfile();
            }

            //=================kiểm tra ngày hợp lệ=========================
            string date = year + "/" + month + "/" + day;
            var a = String.Format("{0:MM/dd/yyyy}", date);
            DateTime birthday;
            if (!DateTime.TryParse(a, out birthday))
            {
                ViewData["Error"] = "Ngày sinh không hợp lệ";
                return this.MyProfile();
            }
            //=============================================================

            user.HoTen = name;
            user.Email = email;
            user.DiachiKH = address;
            user.DienthoaiKH = phoneNumber;
            user.Ngaysinh = birthday;
            db.SaveChanges();
            Session["User"] = user;
            ViewData["Success"] = "Cập nhật thành công";
            return this.MyProfile();
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(FormCollection collection)
        {
            if (Session["User"] == null) RedirectToAction("Login");
            KHACHHANG kh = (KHACHHANG)Session["User"];
            dbShoesStoreDataContext db = new dbShoesStoreDataContext();
            var user = db.KHACHHANGs.SingleOrDefault(p => p.MaKH == kh.MaKH);

            var oldPassword = collection["oldPassword"];
            var newPassword = collection["newPassword"];
            var confirmNewPassword = collection["confirmNewPassword"];

            if (String.IsNullOrEmpty(oldPassword) || String.IsNullOrEmpty(newPassword) || // trống textbox
                String.IsNullOrEmpty(confirmNewPassword))
            {
                ViewData["Error"] = "Vui lòng điền đủ thông tin";
                return this.ChangePassword();
            }
            else if (!String.Equals(newPassword, confirmNewPassword)) // 2 ô mật khẩu mới không khớp
            {
                ViewData["Error"] = "Mật khẩu mới không khớp";
                return this.ChangePassword();
            }
            else if(!String.Equals(MD5Hash(oldPassword),user.Matkhau)) // kiểm tra mật khẩu cũ
            {
                ViewData["Error"] = "Sai mật khẩu cũ";
                return this.ChangePassword();
            }
            else // ==============thay đổi mật khẩu===================
            {
                newPassword = MD5Hash(newPassword);
                user.Matkhau = newPassword;
                db.SaveChanges();
                Session["User"] = user;
                ViewData["Success"] = "Đổi mật khẩu thành công";
                return this.ChangePassword();
            }
        }
    }
}