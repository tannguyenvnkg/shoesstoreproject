﻿////const { EALREADY } = require("constants");
////const { type } = require("jquery");

var admin = {
    init: function () {
        admin.event();
    },
    event: function () {
        // sự kiện xóa text trong modal tạo admin
        $('.create-admin').off('click').on('click', function () {
            admin.resetForm();
        });
        // sự kiện click button thêm admin
        $('#btn-create-admin').off('click').on('click', function () {
            admin.addAdmin();
        });
        //================================
        // modal khóa tải khoản admin
        $('.modal-lock-admin').off('click').on('click', function () {
            var id = $(this).data('account');
            $('#hiddenAccount').val(id);
        });
        $('#btn-lock-admin').off('click').on('click', function () {
            var id = $('#hiddenAccount').val();
            admin.lockOrUnlockAdmin(id);
        });
        //================================
        //modal đổi mật khẩu admin
        $('.modal-change-password').off('click').on('click', function () {
            var id = $(this).data('account');
            $('#hiddenAccount-password').val(id);
        });
        $('#btn-change-password').off('click').on('click', function () {
            var id = $('#hiddenAccount-password').val();
            var password = $('#change-password').val();
            admin.changePassword(id, password);
        });
        //================================
        // modal ngừng kinh doanh sản phẩm
        $('.modal-stop-selling').off('click').on('click', function () {
            var id = $(this).data('product-id');
            $('#productID').val(id);
        });
        $('#btn-stop-selling').off('click').on('click', function () {
            var id = $('#productID').val();
            admin.stopSellingOrSelling(id);
        });
        //================================
        // modal ẩn loại sản phẩm
        $('.modal-hide-category').off('click').on('click', function () {
            var id = $(this).data('category-id');
            $('#categoryID').val(id);
        });
        $('#btn-hide-category').off('click').on('click', function () {
            var id = $('#categoryID').val();
            admin.hideOrUnhideCategory(id);
        });
        //================================
        // modal ẩn nhà cung cấp
        $('.modal-hide-ncc').off('click').on('click', function () {
            var id = $(this).data('ncc-id');
            $('#NCCID').val(id);
        });
        $('#btn-hide-ncc').off('click').on('click', function () {
            var id = $('#NCCID').val();
            admin.hideOrUnhideNCC(id);
        });
        //================================
        //modal đổi mật khẩu admin (admin đăng nhập)
        $('.modal-change-password-admin-login').off('click').on('click', function () {
            var id = $(this).data('admin-login');
            $('#userName-admin-login').val(id);
        });
        $('#btn-change-password-admin-login').off('click').on('click', function () {
            var id = $('#userName-admin-login').val();
            var oldPassword = $('#oldPassword').val();
            var newPassword = $('#newPassword').val();
            var confirmPassword = $('#confirmPassword').val();

            if (oldPassword === '' || newPassword === '' || confirmPassword === '') alert('Mật khẩu không được để trống');
            else if (newPassword !== confirmPassword) alert('Mật khẩu mới không khớp');
            else  admin.changePasswordAdminLogin(id,oldPassword,newPassword);
        });
        //================================
        // modal hoàn thành đơn hàng
        //$('.modal-complete-order').off('click').on('click', function () {
        //    var id = $(this).data('product-id');
        //    $('#productID').val(id);
        //});
        $('#btn-done').off('click').on('click', function () {
            var id = $('.modal-complete-order').data('order-id');
            admin.completeOrder(id);
        });
        //================================
    },

    completeOrder: function (id) {
        $.ajax({
            url: '/Admin/CompleteOrder',
            data: {
                id: id
            },
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                if (response.status) { // trả về giá trị hoàn thành đơn hàng thành công
                    alert(response.successMessage);
                    $('#completeOrder').modal('hide');
                } else { 
                    alert(response.errorMessage);
                }
            },
            error: function (err) {
                console.log(err);
                alert(err.errorMessage)
            }
        });
    },
    changePasswordAdminLogin: function (id,oldPassword,newPassword) {
        $.ajax({
            url: '/Admin/changePasswordAdminLogin',
            data: {
                id: id,
                oldPassword: oldPassword,
                newPassword: newPassword,
            },
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                if (response.status) { // trả về giá trị đổi mật khẩu tài khoản thành công
                    //alert('đổi mật khẩu thành công');
                    $('#change-password-admin-login').modal('hide');
                    alert(response.successMessage)
                } else { // Không thể thay đổi mật khẩu tài khoản của chủ sở hữu
                    alert(response.errorMessage);
                }
            },
            error: function (err) {
                console.log(err);
                alert(err.errorMessage)
            }
        });
    },
    stopSellingOrSelling: function (id) {
        $.ajax({
            url: '/Admin/StopSellingOrSelling',
            data: {
                id: id
            },
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                if (response.status) { // trả về giá trị code chạy thành công
                    if (response.sellingStatus) alert('Đã ngừng kinh doanh sản phẩm');
                    else alert('Đã mở bán sản phẩm');
                    $('#stopSelling').modal('hide');
                    location.reload(); // reload lại trang
                }
            },
            error: function (err) {
                console.log(err);
                alert(err.errorMessage)
            }
        });
    },
    hideOrUnhideCategory: function (id) {
        $.ajax({
            url: '/Admin/HideOrUnhideCategory',
            data: {
                id: id
            },
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                if (response.status) { // trả về giá trị code chạy thành công
                    if (response.sellingStatus) alert('Đã ẩn loại sản phẩm');
                    else alert('Đã mở loại sản phẩm');
                    $('#stopSelling').modal('hide');
                    location.reload(); // reload lại trang
                }
            },
            error: function (err) {
                console.log(err);
                alert(err.errorMessage)
            }
        });
    },
    hideOrUnhideNCC: function (id) {
        $.ajax({
            url: '/Admin/HideOrUnhideNCC',
            data: {
                id: id
            },
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                if (response.status) { // trả về giá trị code chạy thành công
                    if (response.sellingStatus) alert('Đã ẩn nhà cung cấp');
                    else alert('Đã mở nhà cung cấp');
                    $('#stopSelling').modal('hide');
                    location.reload(); // reload lại trang
                }
            },
            error: function (err) {
                console.log(err);
                alert(err.errorMessage)
            }
        });
    },
    changePassword: function (id,password) {
        $.ajax({
            url: '/Admin/ChangePasswordAdmin',
            data: {
                id: id,
                password: password
            },
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                if (response.status) { // trả về giá trị đổi mật khẩu tài khoản thành công
                    alert('đổi mật khẩu thành công');
                    $('#changePassword').modal('hide');
                    //location.reload(); // reload lại trang
                } else { // Không thể thay đổi mật khẩu tài khoản của chủ sở hữu
                    alert(response.exit);
                }
            },
            error: function (err) {
                console.log(err);
                alert(err.errorMessage)
            }
        });
    },
    lockOrUnlockAdmin: function (id) {
        $.ajax({
            url: '/Admin/LockOrUnlockAdmin',
            data: {
                id: id
            },
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                if (response.status) { // trả về giá trị khóa tài khoản thành công
                    if (response.lockStatus) alert('Khóa tài khoản thành công');
                    else alert('mở khóa tài khoản thành công');
                    $('#lockAccount').modal('hide');
                    location.reload(); // reload lại trang
                } else { // khóa lỗi do người bị khóa là chủ sở hữu
                    alert(response.exit);
                }
            },
            error: function (err) {
                console.log(err);
                alert(err.errorMessage)
            }
        });
    },
    addAdmin: function () {
        var userName = $('#userName').val();
        var password = $('#password').val();
        var name = $('#name').val();
        var a = { // đối tượng lưu admin đã nhập trong modal
            Taikhoan: userName,
            Matkhau: password,
            Hoten: name
        };
        $.ajax({
            url: '/Admin/CreateAdmin',
            data: {
                newAdmin: JSON.stringify(a)
            },
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                if (response.status) { // trả về giá trị thêm mới thành công
                    alert('Thêm Admin mới thành công');
                    $('myModal').modal('hide');
                    location.reload(); // reload lại trang
                } else { // đã tồn tại tài khoản
                    alert(response.exit);
                }
            },
            error: function (err) {
                console.log(err);
                alert(err.errorMessage)
            }
        });
    },
    resetForm: function () {
        $('#userName').val('');
        $('#password').val('');
        $('#name').val('');
    }
};

admin.init();